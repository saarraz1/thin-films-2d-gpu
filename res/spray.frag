#version 420

in vec2 coordinate;
layout(binding = 0) uniform sampler2D tex;
out vec4 color;

void main(void)
{
    color = texture(tex, coordinate);
}