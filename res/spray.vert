#version 430

layout(location=0) in vec4 position;
layout(location=1) in vec2 textureCoordinate;
out vec2 coordinate;

void main(void)
{
	gl_Position = position;
	coordinate = textureCoordinate;
}
