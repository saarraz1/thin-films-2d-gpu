#include <glpp/glpp.hpp>
#include <GL/freeglut.h>
#include <optional.hpp>
#include <iostream>
#include <random>
#include <chrono>
#include <thread>
#include <sstream>
#include <numeric>
#include <fstream>

using namespace std::chrono;

namespace std
{
    using experimental::optional;
}

std::string replace(std::string where, const std::string& what, const std::string& with)
{
    std::string::size_type n = 0;
    while ((n = where.find(what, n)) != std::string::npos) {
        where.replace(n, what.size(), with);
        n += with.size();
    }
    return where;
}

struct spray_scene
{
	struct vertex
    {
		GLdouble x;
        GLdouble y;
        GLdouble z;
		struct {
			GLdouble u;
			GLdouble v;
		} textureCoordinate;
	};

    static constexpr std::size_t imageSize = 513;
    static constexpr std::size_t n = imageSize;
    static constexpr std::size_t m = imageSize;
    static constexpr std::size_t iterations = 10;
    static constexpr std::size_t tY = 32;
    static constexpr std::size_t tX = 1024 / tY;
    static constexpr float t = .001;
    static constexpr float h = .01;

    static_assert((tX * tY) <= 1024);
    static_assert((n - 1) % (4 * tX) == 0);
    static_assert((m - 1) % (4 * tY) == 0);

    static std::string getOutputFilename()
    {
        std::stringstream ss;
        ss << n << "x" << m << "_"
           << "t" << t << "_"
           << "h" << h << "_"
           << "i" << iterations << "_"
           << "tx" << tX << "_"
           << "ty" << tY << "_"
           << std::random_device{}() << ".csv";
        return ss.str();
    }

    spray_scene()
        : output(getOutputFilename())
    {
        vertex data[] = {
            { -.5, -.5, 0, {0, 1} },
            {  .5, -.5, 0, {1, 1} },
            {  .5,  .5, 0, {1, 0} },
            { -.5,  .5, 0, {0, 0} }
        };
        dataBuffer.define_data(sizeof(data), data, glpp::buffer_usage_pattern::STATIC_DRAW);

        static_assert(sizeof(vertex) == offsetof(vertex, textureCoordinate) + sizeof(vertex::textureCoordinate));
        vertices.attrib(0)->set_pointerf(
            dataBuffer,
            3,
            glpp::attribf_data_type::DOUBLE,
            sizeof(vertex),
            0
        );
        vertices.attrib(0)->enable();
        vertices.attrib(1)->set_pointerf(
            dataBuffer,
            2,
            glpp::attribf_data_type::DOUBLE,
            sizeof(vertex),
            offsetof(vertex, textureCoordinate)
        );
        vertices.attrib(1)->enable();

        std::cout << "Building render program..." << std::flush;
        renderProgram.attach_shader(glpp::open_shader_file(glpp::shader_type::VERTEX, "res/spray.vert"));
        renderProgram.attach_shader(glpp::open_shader_file(glpp::shader_type::FRAGMENT, "res/spray.frag"));
        renderProgram.build();
        renderProgram.use();
        std::cout << "Done!\n";

        std::cout << "Building compute program..." << std::flush;
        auto computeShader = glpp::open_shader_file(glpp::shader_type::COMPUTE, "res/spray.comp");
        computeShader->source() = replace(computeShader->source(), "#define M 1", "#define M " + std::to_string(m));
        computeShader->source() = replace(computeShader->source(), "#define N 1", "#define N " + std::to_string(n));
        computeShader->source() = replace(computeShader->source(), "#define ITERATIONS 1", "#define ITERATIONS " + std::to_string(iterations));
        computeShader->source() = replace(computeShader->source(), "#define T_X 1", "#define T_X " + std::to_string(tX));
        computeShader->source() = replace(computeShader->source(), "#define T_Y 1", "#define T_Y " + std::to_string(tY));
        computeShader->source() = replace(computeShader->source(), "#define T 1", "#define T " + std::to_string(t));
        computeShader->source() = replace(computeShader->source(), "#define H 1", "#define H " + std::to_string(h));
        //std::cout << computeShader->source() << std::endl;
        computeProgram.attach_shader(computeShader);
        computeProgram.build();
        computeProgram.use();
        std::cout << "Done!\n";

        // Initialize the scene with uniform random.
        static std::array<float, imageSize * imageSize> buf;
        switch (1) {
        // Random
        case 0: {
            std::mt19937 random(std::random_device{}());
            std::uniform_real_distribution<float> distribution(0, 1);
            for (auto &color : buf) {
                color = distribution(random);
            }
            break;
        }
        // Gradient
        case 1:
            std::fill(buf.begin(), buf.end(), 0);
            for (std::size_t i = 0; i < imageSize; ++i) {
                for (std::size_t j = 0; j < imageSize; ++j) {
                    buf[i * imageSize + j] = float(i) / imageSize;
                }
            }
            break;
        // Square
        case 2:
            std::fill(buf.begin(), buf.end(), 0);
            for (std::size_t i = imageSize / 4; i < imageSize - imageSize / 4; ++i) {
                for (std::size_t j = imageSize / 4; j < imageSize - imageSize / 4; ++j) {
                    buf[i * imageSize + j] = 1.;
                }
            }
            break;
        }

        static std::array<float, (imageSize * 2 - 1) * (imageSize * 2 - 1)> workbuf;
        std::fill(workbuf.begin(), workbuf.end(), 0);

        texture2.define_data_2d(
            glpp::tex2d_update_target::TEX_2D,
            /*lvl=*/0,
            glpp::image_format::R32F,
            /*width=*/imageSize * 2 - 1,
            /*height=*/imageSize * 2 - 1,
            /*border=*/0,
            glpp::pixel_format::RED,
            glpp::tex_pixel_type::FLOAT,
            /*pdata=*/workbuf.data()
        );
        texture2.set_min_filter(glpp::min_filter_type::NEAREST);
        texture2.set_mag_filter(glpp::mag_filter_type::NEAREST);
        //glpp::current_ctx().texture_unit(1).bind_texture(texture1);
        glBindImageTexture(
            /*unit=*/1,
            /*texture=*/texture2.object_name(),
            /*level=*/0,
            /*layered=*/GL_FALSE,
            /*layer=*/0,
            /*access=*/GL_READ_WRITE,
            /*format=*/GL_R32F
        );

        texture1.define_data_2d(
            glpp::tex2d_update_target::TEX_2D,
            /*lvl=*/0,
            glpp::image_format::R32F,
            /*width=*/imageSize,
            /*height=*/imageSize,
            /*border=*/0,
            glpp::pixel_format::RED,
            glpp::tex_pixel_type::FLOAT,
            /*pdata=*/buf.data()
        );
        texture1.set_min_filter(glpp::min_filter_type::LINEAR);
        texture1.set_mag_filter(glpp::mag_filter_type::LINEAR);
        //glpp::current_ctx().texture_unit(1).bind_texture(texture1);
        glBindImageTexture(
            /*unit=*/0,
            /*texture=*/texture1.object_name(),
            /*level=*/0,
            /*layered=*/GL_FALSE,
            /*layer=*/0,
            /*access=*/GL_READ_WRITE,
            /*format=*/GL_R32F
        );

        //::glBindTexture(GL_TEXTURE_2D, texture2.object_name());

        std::cerr << "Initialized" << std::endl;
    }

    auto& getImage()
    {
        static std::array<float, imageSize * imageSize> buf;
        glGetTextureImage(
            texture1.object_name(),
            /*level=*/0,
            /*format=*/GL_RED,
            /*type=*/GL_FLOAT,
            sizeof(buf),
            buf.data()
        );
        return buf;
    }

    auto& getWorkImage()
    {
        static std::array<float, (imageSize * 2 - 1) * (imageSize * 2 - 1)> buf;
        glGetTextureImage(
            texture2.object_name(),
            /*level=*/0,
            /*format=*/GL_RED,
            /*type=*/GL_FLOAT,
            sizeof(buf),
            buf.data()
        );
        return buf;
    }

    void draw()
    {
        //std::this_thread::sleep_for(1s);
        high_resolution_clock::time_point before = high_resolution_clock::now();

        renderProgram.use();
        vertices.draw(0, 4);

        computeProgram.use();
        glDispatchCompute(1, 1, 1);
        time += t;

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_TEXTURE_FETCH_BARRIER_BIT);

        const auto& image = getImage();
        output << time << ", " << getEnergy(getWorkImage()) << std::endl;
    }

    static constexpr std::size_t at(std::size_t row, std::size_t column)
    {
        return row * (imageSize * 2 - 1) + column;
    }

    double getEnergy(const std::array<float, (imageSize * 2 - 1) * (imageSize * 2 - 1)>& workImage)
    {
        static std::array<double, (imageSize * 2 - 1) * (imageSize * 2 - 1)> temp;
        std::fill(temp.begin(), temp.end(), 0);
        double fluxTerm = 0;
        for (std::size_t i = 0; i < imageSize * 2 - 1; i += 2) {
            for (std::size_t j = 0; j < imageSize * 2 - 1; j += 2) {
                float u = workImage[at(i, j)];
                if (i != imageSize * 2 - 2) {
                    float downFlux = workImage[at(i + 1, j)];
                    fluxTerm += downFlux * downFlux;
                    temp[at(i + 1, j)] += u;
                }
                if (i != 0) {
                    float upFlux = workImage[at(i - 1, j)];
                    temp[at(i - 1, j)] -= u;
                }
                if (j != imageSize * 2 - 2) {
                    float rightFlux = workImage[at(i, j + 1)];
                    fluxTerm += rightFlux * rightFlux;
                    temp[at(i, j + 1)] += u;
                }
                if (j != 0) {
                    float leftFlux = workImage[at(i, j - 1)];
                    temp[at(i, j - 1)] -= u;
                }
            }
        }
        double uTerm = 0;
        for (std::size_t i = 0; i < imageSize * 2 - 1; i += 2) {
            for (std::size_t j = 0; j < imageSize * 2 - 1; j += 2) {
                if (i != imageSize * 2 - 2) {
                    double downTerm = temp[at(i + 1, j)];
                    uTerm += downTerm * downTerm;
                }
                if (j != imageSize * 2 - 2) {
                    double rightTerm = temp[at(i, j + 1)];
                    uTerm += rightTerm * rightTerm;
                }
            }
        }
        return (t / 2) * ((h * h) / 2) * fluxTerm + 0.5 * uTerm;
    }

    glpp::buffer
        dataBuffer{ glpp::buffer_type::ARRAY };

    glpp::vertex_array
        vertices{ glpp::primitive_type::TRIANGLE_FAN };

    glpp::program
        renderProgram;

    glpp::program
        computeProgram;

    glpp::texture
        texture1{ glpp::texture_type::TEX_2D };

    glpp::texture
        texture2{ glpp::texture_type::TEX_2D };

    double
        time = 0;

    std::ofstream
        output;
};

std::optional<spray_scene>
    scene;

int main(int argv, char** argc)
try {
	glutInit(&argv, argc);
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(512, 512);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutCreateWindow("Thin Films GPU");

    glpp::context::create();

	if (GLuint err = glewInit()) {
		std::cerr << "Could not initialize GLEW. " << glewGetErrorString(err) << "\n";
		return -1;
	}
    if (!GLEW_ARB_shader_image_load_store) {
        std::cerr << "Hardware not supported! GL extension GL_ARB_shader_image_load_store is required." << '\n';
        return -1;
    }

    GLint maxTotalBatchSize = 0;
    glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &maxTotalBatchSize);
    if (maxTotalBatchSize < 1024) {
        throw
            std::runtime_error(
                "Assumed batch size limit to be 1024, but it is lower: " + std::to_string(maxTotalBatchSize)
            );
    } else if (maxTotalBatchSize > 1024) {
        std::cerr << "Warning: Batch size limit is higher than assumed - better performance might be possible.\n";
    }

    scene.emplace();

	glutDisplayFunc(
        [] {

            static duration<double> averageFrameTime{ 0 };
            static auto lastFrameTime = high_resolution_clock::now();
            static auto lastPrintTime = high_resolution_clock::now();

            auto frameTime = high_resolution_clock::now();
            averageFrameTime = averageFrameTime * 0.9 + (frameTime - lastFrameTime) * 0.1;
            lastFrameTime = frameTime;
            if (high_resolution_clock::now() - lastPrintTime >= 1s) {
                std::cout << "Framerate: " << (duration<double>(1) / averageFrameTime) << "fps" << std::endl;
                lastPrintTime = high_resolution_clock::now();
            }

            glClearColor(0, 1, 0, 1);
            glClear(GL_COLOR_BUFFER_BIT);

            scene->draw();
            glutSwapBuffers();
        }
    );

	glutIdleFunc([] { ::glutPostRedisplay(); });
	glutMainLoop();

	return 0;
} catch (std::exception& exception) {
    std::cerr << "ERROR: " << exception.what() << '\n';
    return 1;
}